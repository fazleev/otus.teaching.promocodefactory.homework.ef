﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class PcfContext : DbContext
    {
        protected DbSet<Employee> Employees { get; set; }
        protected DbSet<Role> Roles { get; set; }
        protected DbSet<Customer> Customers { get; set; }
        protected DbSet<Preference> Preferences { get; set; }
        protected DbSet<PromoCode> PromoCodes { get; set; }

        public PcfContext(DbContextOptions<PcfContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasKey(x => x.Id);
            modelBuilder.Entity<Employee>().HasKey(x => x.Id);
            modelBuilder.Entity<Role>().HasKey(x => x.Id);
            modelBuilder.Entity<Preference>().HasKey(x => x.Id);
            modelBuilder.Entity<PromoCode>().HasKey(x => x.Id);

            modelBuilder.Entity<CustomerPreference>()
                .HasKey(cp => new { cp.CustomerId, cp.PreferenceId });

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.Preferences)
                .HasForeignKey(cp => cp.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Preference)
                .WithMany()
                .HasForeignKey(cp => cp.PreferenceId);

            modelBuilder.Entity<Employee>()
                .HasOne(e => e.Role);

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.PromoCodes)
                .WithOne(pc => pc.Customer);
        }
    }
}

