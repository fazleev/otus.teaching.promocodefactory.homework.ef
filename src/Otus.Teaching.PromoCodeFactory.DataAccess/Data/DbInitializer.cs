﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbInitializer
    {
        public static void Initialize(PcfContext context)
        {
            if (context.Database.EnsureCreated())
            {
                context.AddRange(FakeDataFactory.Employees);
                context.AddRange(FakeDataFactory.Preferences);
                context.AddRange(FakeDataFactory.Customers);
                context.SaveChanges();
            }

            context.Database.Migrate();
        }
    }
}
