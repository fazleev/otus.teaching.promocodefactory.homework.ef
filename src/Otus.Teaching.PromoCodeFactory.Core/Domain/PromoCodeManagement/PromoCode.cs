﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [Required]
        [MaxLength(128)]
        public string Code { get; set; }

        [Required]
        [MaxLength(1024)]
        public string ServiceInfo { get; set; }

        [Required]
        public DateTime BeginDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        [MaxLength(128)]
        public string PartnerName { get; set; }

        [Required]
        public virtual Employee PartnerManager { get; set; }

        [Required]
        public virtual Preference Preference { get; set; }

        [Required]
        public virtual Customer Customer { get; set; }

        public static string GenerateNewCode()
        {
            var code = string.Empty;

            var rnd = new Random();

            for (int i = 0; i < 5; i++)
            {
                code += (char)rnd.Next(65, 90);
            }

            return code;
        }
    }
}