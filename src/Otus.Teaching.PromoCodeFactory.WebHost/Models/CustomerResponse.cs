﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public CustomerResponse(Customer customer)
        {
            Id = customer.Id;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
            Preferences = customer.Preferences.Select(x => new CustomerPreferenceResponse(x));
            PromoCodes = customer.PromoCodes.Select(x => new PromoCodeShortResponse(x));
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public IEnumerable<CustomerPreferenceResponse> Preferences { get; set; }

        public IEnumerable<PromoCodeShortResponse> PromoCodes { get; set; }
    }
}