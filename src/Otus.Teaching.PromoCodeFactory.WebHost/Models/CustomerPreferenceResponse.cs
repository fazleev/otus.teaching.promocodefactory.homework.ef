﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerPreferenceResponse
    {
        public CustomerPreferenceResponse(CustomerPreference customerPreference)
        {
            Id = customerPreference.Preference.Id;
            Name = customerPreference.Preference.Name;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
