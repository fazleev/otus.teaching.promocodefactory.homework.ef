﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public PromoCodesController(IRepository<PromoCode> promoCodeRepository, IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository, IRepository<Employee> employeeRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromoCodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            var result = promoCodes.Select(x =>
                new PromoCodeShortResponse
                {
                    Id = x.Id,
                    Code = x.Code,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString("dd-MM-yy"),
                    EndDate = x.EndDate.ToString("dd-MM-yy"),
                    PartnerName = x.PartnerName
                });
            return Ok(result);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var customers = await _customerRepository.GetAllAsync();
            var preference = await _preferenceRepository.GetByIdAsync(Guid.Parse(request.Preference));
            var partnerManager = await _employeeRepository.GetByIdAsync(Guid.Parse(request.PartnerManager));
            var beginDate = DateTime.Now;
            var endDate = DateTime.Now.AddDays(30);

            foreach (var customer in customers)
            {
                if(!customer.Preferences.Any(x => x.Preference.Id == preference.Id)) continue;

                var promoCode = new PromoCode
                {
                    Code = PromoCode.GenerateNewCode(),
                    ServiceInfo = request.ServiceInfo,
                    BeginDate = beginDate,
                    EndDate = endDate,
                    PartnerName = request.PartnerName,
                    PartnerManager = partnerManager,
                    Preference = preference,
                    Customer = customer
                };

                await _promoCodeRepository.AddAsync(promoCode);
            }

            return Ok();
        }
    }
}